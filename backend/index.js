var express = require('express');
var request = require('request');
var cors = require('cors')
var app = express();
app.use(cors());

let query = [
	"limit=1",
	"game=Gigantic",
	"period=month",
	"broadcast_type=archive",
	"language=en",
	"sort=time",
].join("&");
let url = "https://api.twitch.tv/kraken/videos/top?" + query;

app.get("/twitch", function(req, res){
	const options = {
		url: url,
		headers: {
			"Client-ID": "7kxrmbwleytzzv0tb3ppbpfggmw5ii",
			"Accept": "application/vnd.twitchtv.v5+json",
		},
	};

	function callback(error, response, body) {
		if (!error && response.statusCode == 200) {
			var info = JSON.parse(body);
			res.end(body);
		} else {
			res.json({error: 123});
		}
	}

	request(options, callback);
});

app.listen(3000);
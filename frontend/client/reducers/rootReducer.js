import { combineReducers } from "redux";
import stuff from "./stuffReducer";
import recentVideos from "./recentVideosReducer";

const rootReducer = combineReducers({
	stuff,
	recentVideos,
});

export default rootReducer;
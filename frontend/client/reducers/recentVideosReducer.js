import initialState from "./initialState";
import { FETCH_RECENT_VIDEOS, RECEIVE_RECENT_VIDEOS } from "../actions/actionTypes";

export default function recentVideos(state = initialState.recentVideos, action) {
	let newState;
	switch (action.type) {
	case FETCH_RECENT_VIDEOS:
		console.log("FETCH_STUFF Action")
		return action;
	case RECEIVE_RECENT_VIDEOS:
		newState = action.recentVideos;
		console.log("RECEIVE_STUFF Action")
		return newState;
	default:
		return state;
	}
}
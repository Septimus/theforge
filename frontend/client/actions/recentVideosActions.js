import * as types from "./actionTypes";

function url() {
	return "http://localhost:3000/twitch";
}

export function receiveRecentVideos(json) {
	let recentVideos = [];
	recentVideos.push({url: "https://www.twitch.tv/videos/186127757", thumbnail: "https://static-cdn.jtvnw.net/s3_vods/27f61c3d9312c314e91c_trinatis_266\n" +
	"04484224_731450455/thumb/thumb0-640x360.jpg"});
	return { type: types.RECEIVE_RECENT_VIDEOS, recentVideos: recentVideos };
}

export function fetchRecentVideos() {
	return dispatch => {
		return fetch(url(), {
			method: "GET",
		})
			.then(response => {
				response.json().then((json) => {
					dispatch(receiveRecentVideos(json))
				});
			});
	};
}

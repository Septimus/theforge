module.exports = {
	FETCH_STUFF: "FETCH_STUFF",
	RECEIVE_STUFF: "RECEIVE_STUFF",
	FETCH_RECENT_VIDEOS: "FETCH_RECENT_VIDEOS",
	RECEIVE_RECENT_VIDEOS: "RECEIVE_RECENT_VIDEOS",
}

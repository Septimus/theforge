import {connect} from "react-redux"
import React from "react"

class sidebar extends React.Component {
	render() {
		return (
			<div>
				<h1>The Forge</h1>
			</div>
		)
	}
}

sidebar.propTypes = {

}

function mapStateToProps(state) {
	return {

	}
}

function mapDispatchToProps(dispatch) {
	return {

	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(sidebar)
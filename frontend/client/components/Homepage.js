import {connect} from "react-redux"
import {bindActionCreators} from "redux"
import * as _recentVideosActions from "../actions/recentVideosActions"
import PropTypes from "prop-types"
import React from "react"

import Sidebar from "./Sidebar.js";

class homepage extends React.Component {
	renderData() {
		return <div>
			{
				this.props.recentVideos.map((recentVideo) => {
					return <a href={ recentVideo.url }>
						<img src={ recentVideo.thumbnail }/>
					</a>;
				})
			}
		</div>;
	}

	componentDidMount() {
		this.props.recentVideosActions.fetchRecentVideos();
	}

	render() {
		return (
			<main>
				<Sidebar />
				<div>
					<h2>About</h2>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit diam at tellus
						condimentum finibus. Sed posuere neque ac mi mollis congue. Nam lectus purus, lobortis sit amet
						commodo sit amet, varius sed magna. Integer at auctor lacus. Nullam sed tempor tortor. Etiam
						vestibulum dui quis viverra mattis. Praesent vestibulum vestibulum lacus sed ultricies. Aenean
						aliquet elit eget felis blandit consequat. Sed aliquet tristique tellus, a hendrerit ligula
						vehicula ut. Quisque viverra vitae orci quis consequat.
					</p>
				</div>
				<div>
					<h2>Last Tournament Broadcasts</h2>
					<button>Left</button>
					{ this.props.recentVideos.length > 0 ?
						this.renderData()
						:
						<div>
							No Data
						</div>
					}
					<button>Right</button>
				</div>
			</main>
		)
	}
}

homepage.propTypes = {
	recentVideoActions: PropTypes.object,
	recentVideos: PropTypes.array,
}

function mapStateToProps(state) {
	console.log(state);
	return {
		recentVideos: state.recentVideos,
	}
}

function mapDispatchToProps(dispatch) {
	return {
		recentVideosActions: bindActionCreators(_recentVideosActions, dispatch),
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(homepage)
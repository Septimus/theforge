import React from "react";
import StuffList from "./StuffList.js";
import Homepage from "./Homepage.js";

export default class App extends React.Component {
	render() {
		return (
			<div style={ { textAlign: "center" } }>
				<Homepage />
			</div>);
	}
}